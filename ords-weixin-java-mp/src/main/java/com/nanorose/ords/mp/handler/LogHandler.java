package com.nanorose.ords.mp.handler;


import me.chanjar.weixin.common.session.WxSessionManager;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.bean.message.WxMpXmlMessage;
import me.chanjar.weixin.mp.bean.message.WxMpXmlOutMessage;

import java.util.Map;

public class LogHandler extends AbstractHandler {

  public LogHandler(){

  }

  @Override
  public WxMpXmlOutMessage handle(WxMpXmlMessage wxMessage,
                                  Map<String, Object> context, WxMpService wxMpService,
                                  WxSessionManager sessionManager) {
    this.logger.info(()-> String.format( "\n接收到请求消息，内容：%s", wxMessage.toString()) );

    return null;
  }

}