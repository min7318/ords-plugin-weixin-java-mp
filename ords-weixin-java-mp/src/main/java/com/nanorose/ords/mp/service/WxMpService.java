package com.nanorose.ords.mp.service;

import com.nanorose.ords.mp.config.WxMpConfig;
import com.nanorose.ords.mp.handler.*;
import me.chanjar.weixin.mp.api.WxMpInMemoryConfigStorage;
import me.chanjar.weixin.mp.api.WxMpMessageRouter;
import me.chanjar.weixin.mp.api.impl.WxMpServiceImpl;
import me.chanjar.weixin.mp.bean.kefu.result.WxMpKfOnlineList;
import me.chanjar.weixin.mp.bean.message.WxMpXmlMessage;
import me.chanjar.weixin.mp.bean.message.WxMpXmlOutMessage;
import me.chanjar.weixin.mp.constant.WxMpEventConstants;
import java.util.logging.Logger;

import static me.chanjar.weixin.common.api.WxConsts.*;

public class WxMpService extends WxMpServiceImpl {
  private final Logger logger = Logger.getLogger(this.getClass().toString());

  protected LogHandler logHandler = new LogHandler();
  
  protected NullHandler nullHandler = new NullHandler();

  protected KfSessionHandler kfSessionHandler = new KfSessionHandler();

  protected StoreCheckNotifyHandler storeCheckNotifyHandler = new StoreCheckNotifyHandler();

  private WxMpConfig wxConfig;

  private LocationHandler locationHandler = new LocationHandler();

  private MenuHandler menuHandler = new MenuHandler();

  private MsgHandler msgHandler = new MsgHandler();

  private UnsubscribeHandler unsubscribeHandler = new UnsubscribeHandler();

  private SubscribeHandler subscribeHandler = new SubscribeHandler();

  private ScanHandler scanHandler = new ScanHandler();

  private WxMpMessageRouter router;

  private String schema ;
  public WxMpService(String schema, WxMpConfig wxConfig) {
    this.schema = schema;
    this.wxConfig = wxConfig;
    init();
  }

  private void init() {
    final WxMpInMemoryConfigStorage config = new WxMpInMemoryConfigStorage();
    // 设置微信公众号的appid
    config.setAppId(this.wxConfig.getAppid(this.schema));
    // 设置微信公众号的app corpSecret
    config.setSecret(this.wxConfig.getAppSecret(this.schema));
    // 设置微信公众号的token
    config.setToken(this.wxConfig.getToken(this.schema));
    // 设置消息加解密密钥
    config.setAesKey(this.wxConfig.getAesKey(this.schema));
    super.setWxMpConfigStorage(config);
    // 按照配置选择对象子类
    String className="";
    className=this.wxConfig.getKfSessionHandler(this.schema);
    if(!("".equals(className))){
      try{
        this.kfSessionHandler = (KfSessionHandler) Class.forName(className).newInstance();
        this.logger.info( String.format( "\nKfSessionHandler：%s", className) );
      }catch(Exception e){
        this.logger.severe(e.getMessage());
      }
    }
    className=this.wxConfig.getLocationHandler(this.schema);
    if(!("".equals(className))){
      try{
        this.locationHandler = (LocationHandler) Class.forName(className).newInstance();
        this.logger.info( String.format( "\nLocationHandler：%s", className) );
      }catch(Exception e){
        this.logger.severe(e.getMessage());
      }
    }
    className=this.wxConfig.getLogHandler(this.schema);
    if(!("".equals(className))){
      try{
        this.logHandler = (LogHandler) Class.forName(className).newInstance();
        this.logger.info( String.format( "\nLogHandler：%s", className) );
      }catch(Exception e){
        this.logger.severe(e.getMessage());
      }
    }
    className=this.wxConfig.getMenuHandler(this.schema);
    if(!("".equals(className))){
      try{
        this.menuHandler = (MenuHandler) Class.forName(className).newInstance();
        this.logger.info( String.format( "\nMenuHandler：%s", className) );
      }catch(Exception e){
        this.logger.severe(e.getMessage());
      }
    }
    className=this.wxConfig.getMsgHandler(this.schema);
    if(!("".equals(className))){
      try{
        this.msgHandler = (MsgHandler) Class.forName(className).newInstance();
        this.logger.info( String.format( "\nMsgHandler：%s", className) );
      }catch(Exception e){
        this.logger.severe(e.getMessage());
      }
    }
    className=this.wxConfig.getScanHandler(this.schema);
    if(!("".equals(className))){
      try{
        this.scanHandler = (ScanHandler) Class.forName(className).newInstance();
        this.logger.info( String.format( "\nScanHandler：%s", className) );
      }catch(Exception e){
        this.logger.severe(e.getMessage());
      }
    }
    className=this.wxConfig.getStoreCheckNotifyHandler(this.schema);
    if(!("".equals(className))){
      try{
        this.storeCheckNotifyHandler = (StoreCheckNotifyHandler) Class.forName(className).newInstance();
        this.logger.info( String.format( "\nStoreCheckNotifyHandler：%s", className) );
      }catch(Exception e){
        this.logger.severe(e.getMessage());
      }
    }
    className=this.wxConfig.getSubscribeHandler(this.schema);
    if(!("".equals(className))){
      try{
        this.subscribeHandler = (SubscribeHandler) Class.forName(className).newInstance();
        this.logger.info( String.format( "\nSubscribeHandler：%s", className) );
      }catch(Exception e){
        this.logger.severe(e.getMessage());
      }
    }
    className=this.wxConfig.getUnsubscribeHandler(this.schema);
    if(!("".equals(className))){
      try{
        this.unsubscribeHandler = (UnsubscribeHandler) Class.forName(className).newInstance();
        this.logger.info( String.format( "\nUnsubscribeHandler：%s", className) );
      }catch(Exception e){
        this.logger.severe(e.getMessage());
      }
    }

    this.refreshRouter();
  }

  private void refreshRouter() {
    final WxMpMessageRouter newRouter = new WxMpMessageRouter(this);

    // 记录所有事件的日志
    newRouter.rule().handler(this.logHandler).next();

    // 接收客服会话管理事件
    newRouter.rule().async(false).msgType(XmlMsgType.EVENT)
      .event(WxMpEventConstants.CustomerService.KF_CREATE_SESSION)
      .handler(this.kfSessionHandler).end();
    newRouter.rule().async(false).msgType(XmlMsgType.EVENT)
      .event(WxMpEventConstants.CustomerService.KF_CLOSE_SESSION)
      .handler(this.kfSessionHandler).end();
    newRouter.rule().async(false).msgType(XmlMsgType.EVENT)
      .event(WxMpEventConstants.CustomerService.KF_SWITCH_SESSION)
      .handler(this.kfSessionHandler).end();

    // 门店审核事件
    newRouter.rule().async(false).msgType(XmlMsgType.EVENT)
      .event(WxMpEventConstants.POI_CHECK_NOTIFY)
      .handler(this.storeCheckNotifyHandler)
      .end();

    // 自定义菜单事件
    newRouter.rule().async(false).msgType(XmlMsgType.EVENT)
      .event(MenuButtonType.CLICK).handler(this.menuHandler).end();

    // 点击菜单连接事件
    newRouter.rule().async(false).msgType(XmlMsgType.EVENT)
      .event(MenuButtonType.VIEW).handler(this.menuHandler).end();

    // 关注事件
    newRouter.rule().async(false).msgType(XmlMsgType.EVENT)
      .event(EventType.SUBSCRIBE).handler(this.subscribeHandler)
      .end();

    // 取消关注事件
    newRouter.rule().async(false).msgType(XmlMsgType.EVENT)
      .event(EventType.UNSUBSCRIBE).handler(this.unsubscribeHandler)
      .end();

    // 上报地理位置事件
    newRouter.rule().async(false).msgType(XmlMsgType.EVENT)
      .event(EventType.LOCATION).handler(this.locationHandler).end();

    // 接收地理位置消息
    newRouter.rule().async(false).msgType(XmlMsgType.LOCATION)
      .handler(this.locationHandler).end();

    // 扫码事件
    newRouter.rule().async(false).msgType(XmlMsgType.EVENT)
      .event(EventType.SCAN).handler(this.scanHandler).end();

    // 默认
    newRouter.rule().async(false).handler(this.msgHandler).end();

    this.router = newRouter;
  }

  public WxMpXmlOutMessage route(WxMpXmlMessage message) {
    try {
      return this.router.route(message);
    } catch (Exception e) {
      this.logger.severe(e.getMessage());
    }

    return null;
  }

  public boolean hasKefuOnline() {
    try {
      WxMpKfOnlineList kfOnlineList = this.getKefuService().kfOnlineList();
      return kfOnlineList != null && kfOnlineList.getKfOnlineList().size() > 0;
    } catch (Exception e) {
      this.logger.severe("获取客服在线状态异常: " + e.getMessage());
    }

    return false;
  }

}