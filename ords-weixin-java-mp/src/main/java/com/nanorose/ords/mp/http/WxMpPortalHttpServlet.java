package com.nanorose.ords.mp.http;

import com.nanorose.ords.mp.service.WxMpService;
import com.nanorose.ords.mp.config.WxMpConfig;
import me.chanjar.weixin.mp.bean.message.WxMpXmlMessage;
import me.chanjar.weixin.mp.bean.message.WxMpXmlOutMessage;
import org.apache.commons.lang3.StringUtils;
import java.util.logging.Logger;
import java.io.IOException;
import java.io.BufferedOutputStream;
import java.sql.*;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import oracle.dbtools.plugin.api.di.annotations.Provides;
import oracle.dbtools.plugin.api.http.annotations.*;
import oracle.dbtools.plugin.api.routes.*;
import com.google.common.io.CharStreams;
import java.sql.Clob;
import me.chanjar.weixin.mp.util.crypto.WxMpCryptUtil;

@Provides
@Dispatches(@PathTemplate(name="portal", value="/wechat/portal",methods={"GET", "POST"})  )
public class WxMpPortalHttpServlet extends HttpServlet {
  public static final String MSG_TABLE_NAME = "wxmsg";

  private final Connection    conn;
  private final PathTemplates pathTemplates;
  private WxMpService wxMpService ;
  private final Logger logger = Logger.getLogger(this.getClass().toString());
  private static final WxMpConfig wxMpConfig = new WxMpConfig();

  @Inject
  public WxMpPortalHttpServlet(Connection conn, PathTemplates pathTemplates) {
    this.conn = conn;
    this.pathTemplates = pathTemplates;
  }
  
  private String getSchema(PathTemplateMatch match) throws SQLException{
    /* execute database query */
    PreparedStatement ps = conn
      .prepareStatement("select sys_context('USERENV','SESSION_USER') from dual");
      
    ResultSet rs = ps.executeQuery();
    rs.next();
    
    /* determine the database user */
    String user = rs.getString(1);
    
    
    rs.close();
    ps.close();
    return user;
  }
  
  private boolean checkSchema(HttpServletRequest request, 
                    HttpServletResponse response){
    PathTemplateMatch match = pathTemplates
                                .matchedTemplate(request);
    String schema = "";                            
    try {
      schema = getSchema(match);
    }catch(SQLException e){
      this.logger.severe(e.getMessage());
      response.setStatus(HttpServletResponse.SC_FORBIDDEN);
      return false;
    }
    if(schema.length()==0){
      this.logger.info("数据库没有对应schema");
      response.setStatus(HttpServletResponse.SC_FORBIDDEN);
      return false;
    }else {
      wxMpService = wxMpConfig.getWxMpService(schema);
      if(wxMpService == null){
         this.logger.info("数据库没有配置 schema=["+schema+"]");
         response.setStatus(HttpServletResponse.SC_FORBIDDEN);
         return false;
      }else{
        return true;
      }
    }  
  }

  @Override
  public void doGet(HttpServletRequest request, 
                    HttpServletResponse response)
      throws ServletException, IOException {

    if(!checkSchema(request,response)){
       return;
    }  
                          
    String signature = "";
    String timestamp = "";
    String nonce = "";
    String echostr = "";
    String mimeType = "text/plain;charset=UTF-8";
    String[] tmpStrings = null;

    tmpStrings = request.getParameterValues("signature");
    if(tmpStrings.length==1){
      signature = tmpStrings[0];
    }
    tmpStrings = request.getParameterValues("timestamp");
    if(tmpStrings.length==1){
      timestamp = tmpStrings[0];
    }
    tmpStrings = request.getParameterValues("nonce");
    if(tmpStrings.length==1){
      nonce = tmpStrings[0];
    }
    tmpStrings = request.getParameterValues("echostr");
    if(tmpStrings.length==1){
      echostr = tmpStrings[0];
    }    
    response.setContentType(mimeType);
                                 
    this.logger.info(
          String.format("\n接收到来自微信服务器的认证消息：[%s, %s, %s, %s]", signature, timestamp, nonce, echostr) );

    if (StringUtils.isAnyBlank(signature, timestamp, nonce, echostr)) {
      this.logger.severe("请求参数非法，请核实!");
      response.setStatus(HttpServletResponse.SC_FORBIDDEN);

    }

    if (wxMpService.checkSignature(timestamp, nonce, signature)) {
      /*
      *处理业务完毕
      */
      BufferedOutputStream out = new BufferedOutputStream(
            response.getOutputStream());
      out.write(echostr.getBytes("UTF-8"));
      out.flush();
      out.close();
    }else{
      this.logger.info("非法请求，可能属于伪造的请求！");
      response.setStatus(HttpServletResponse.SC_FORBIDDEN);
    }
  }
  
  @Override
  public void doPost(HttpServletRequest request, 
                    HttpServletResponse response)
      throws ServletException, IOException {
    if(!checkSchema(request,response)){
       return;
    }  
    String signature = ""; //signature
    String encType = "";  //encrypt_type
    String msgSignature = ""; //msg_signature
    String timestamp = "";
    String nonce = ""; 
    String requestBody = "";
    String mimeType = "application/xml;charset=UTF-8";
    String[] tmpStrings = null;
    
    tmpStrings = request.getParameterValues("signature");
    if(tmpStrings.length==1){
      signature = tmpStrings[0];
    }
    tmpStrings = request.getParameterValues("encrypt_type");
    if(tmpStrings.length==1){
      encType = tmpStrings[0];
    }
    tmpStrings = request.getParameterValues("msg_signature");
    if(tmpStrings.length==1){
      msgSignature = tmpStrings[0];
    }
    tmpStrings = request.getParameterValues("timestamp");
    if(tmpStrings.length==1){
      timestamp = tmpStrings[0];
    }
    tmpStrings = request.getParameterValues("nonce");
    if(tmpStrings.length==1){
      nonce = tmpStrings[0];
    }
    requestBody = CharStreams.toString(request.getReader());
    response.setContentType(mimeType);
    this.logger.info(  String.format("\n接收微信请求：[signature=[%s], encType=[%s], msgSignature=[%s],"
        + " timestamp=[%s], nonce=[%s], requestBody=[\n%s\n] ",
      signature, encType, msgSignature, timestamp, nonce, requestBody) );

    if (StringUtils.isAnyBlank(signature, timestamp, nonce)) {
      throw new IllegalArgumentException("请求参数非法，请核实!");
    }
    if (!wxMpService.checkSignature(timestamp, nonce, signature)) {
        this.logger.info("非法请求，可能属于伪造的请求！");
        response.setStatus(HttpServletResponse.SC_FORBIDDEN);
    }
    else {
      String out = null;
      WxMpCryptUtil cryptUtil = new WxMpCryptUtil(wxMpService.getWxMpConfigStorage());
      String plainText = "";
      if (encType == null) {
        plainText = requestBody;
        try{
          saveToDatabase(plainText);
        }catch(SQLException e){
          this.logger.info(e.getMessage());
        }
        // 明文传输的消息
        WxMpXmlMessage inMessage = WxMpXmlMessage.fromXml(requestBody);
        WxMpXmlOutMessage outMessage = wxMpService.route(inMessage);
        if (outMessage == null) {
          return;
        }

        out = outMessage.toXml();
      } else if ("aes".equals(encType)) {
        plainText = cryptUtil.decrypt(msgSignature, timestamp, nonce, requestBody);
        try{
          saveToDatabase(plainText);
        }catch(SQLException e){
          this.logger.info(e.getMessage());
        }
        // aes加密的消息
        WxMpXmlMessage inMessage = WxMpXmlMessage.fromEncryptedXml(requestBody,
          wxMpService.getWxMpConfigStorage(), timestamp, nonce, msgSignature);
        this.logger.finest( String.format( "\n消息解密后内容为：\n%s", inMessage.toString() ) );
        WxMpXmlOutMessage outMessage = wxMpService.route(inMessage);
        if (outMessage == null) {
          return;
        }
        
        out = outMessage.toEncryptedXml(wxMpService.getWxMpConfigStorage());
      }
      if(out==null){
        this.logger.info("消息无内容");
        return;
      }
      this.logger.finest( String.format( "\n组装回复信息：%s", out) );
      /*
       *处理业务完毕
       */
      BufferedOutputStream outStream = new BufferedOutputStream(
           response.getOutputStream());
      outStream.write(out.getBytes("UTF-8"));
      outStream.flush();
      outStream.close();
    }      
  }

  private void saveToDatabase(String str) throws SQLException{
    Clob clob = conn.createClob();
    clob.setString(1,str);
        /* execute database query */
    PreparedStatement ps = conn
      .prepareStatement("insert into " + MSG_TABLE_NAME + "(msg,msg_time) values (?,systimestamp)");
    ps.setClob(1,clob);
    ps.executeUpdate();
    conn.commit();
    ps.close();
    clob.free();
  }
}