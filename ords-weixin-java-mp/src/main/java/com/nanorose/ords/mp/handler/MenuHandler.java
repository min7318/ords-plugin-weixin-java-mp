package com.nanorose.ords.mp.handler;

import com.nanorose.ords.mp.builder.TextBuilder;
import me.chanjar.weixin.common.api.WxConsts;
import me.chanjar.weixin.common.session.WxSessionManager;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.bean.message.WxMpXmlMessage;
import me.chanjar.weixin.mp.bean.message.WxMpXmlOutMessage;

import java.util.Map;

public class MenuHandler extends AbstractHandler {

  public MenuHandler(){
    
  }
  @Override
  public WxMpXmlOutMessage handle(WxMpXmlMessage wxMessage,
                                  Map<String, Object> context, WxMpService weixinService,
                                  WxSessionManager sessionManager) {
    String msg = String.format("type:%s, event:%s, key:%s",
      wxMessage.getMsgType(), wxMessage.getEvent(),
      wxMessage.getEventKey());
    if (WxConsts.MenuButtonType.VIEW.equals(wxMessage.getEvent())) {
      return null;
    }
    return new TextBuilder().build(msg, wxMessage, null);
    
  }


}