package com.nanorose.ords.mp.config;

import java.util.Properties;
import java.io.StringReader;
import java.io.IOException;
import com.nanorose.ords.mp.service.WxMpService;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.InputStream;
import java.util.Hashtable;

public class WxMpConfig {
  public static final String configFile = "ords-plugin-weixin-java-mp.properties";

  private Hashtable<String,WxMpService> wxMpServices = new Hashtable<String,WxMpService>();

  private Properties prop = new Properties();

  public WxMpConfig(){
    String propContent = "";
    InputStreamReader reader = null;
    InputStream input = null;
    try {
       input = WxMpConfig.class.getClassLoader().getResourceAsStream(configFile);
       reader = new InputStreamReader( input, "UTF-8");
       updateConfiguration(reader);
    }catch(IOException e){
      
    }finally{
      try{
        if(input!=null){
          input.close();
        }
        if(reader!=null){
          reader.close();
        }
      }catch(IOException e){

      }
    }
  }

  private void updateConfiguration(Reader reader) throws IOException{
    prop.clear();
    prop.load(reader);
    wxMpServices.clear();
    String[] schemas= prop.getProperty("schemas","").split(",");
    for( int i=0;i<schemas.length;i++){
      String schema = schemas[i].trim();
      wxMpServices.put(schema,new WxMpService(schema,this) ); 
    }
  }
  public void updateConfiguration(String propContent){
    try{
      StringReader reader = new StringReader(propContent);
      updateConfiguration(reader);
    }catch(IOException e){

    }
  }

  public WxMpService getWxMpService(String schema){
    if(!wxMpServices.containsKey(schema)){
      return null; 
    }
    return wxMpServices.get(schema);
  }
  public String getToken(String schema) {
    return this.prop.getProperty(schema+".token","");
  }

  public String getAppid(String schema) {
    return this.prop.getProperty(schema+".appid","");
  }

  public String getAppSecret(String schema) {
    return this.prop.getProperty(schema+".appSecret","");
  }

  public String getAesKey(String schema) {
    return this.prop.getProperty(schema+".aesKey","");
  }

  public String getLogHandler(String schema){
    return this.prop.getProperty(schema+".logHandler","");
  }

  public String getKfSessionHandler(String schema){
    return this.prop.getProperty(schema+".kfSessionHandler","");
  }

  public String getLocationHandler(String schema){
    return this.prop.getProperty(schema+".locationHandler","");
  }

  public String getMenuHandler(String schema){
    return this.prop.getProperty(schema+".menuHandler","");
  }

  public String getMsgHandler(String schema){
    return this.prop.getProperty(schema+".msgHandler","");
  }

  public String getScanHandler(String schema){
    return this.prop.getProperty(schema+".scanHandler","");
  }

  public String getStoreCheckNotifyHandler(String schema){
    return this.prop.getProperty(schema+".storeCheckNotifyHandler","");
  }

  public String getSubscribeHandler(String schema){
    return this.prop.getProperty(schema+".subscribeHandler","");
  }

  public String getUnsubscribeHandler(String schema){
    return this.prop.getProperty(schema+".unsubscribeHandler","");
  }

}