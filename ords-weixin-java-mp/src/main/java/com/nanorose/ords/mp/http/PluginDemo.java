package com.nanorose.ords.mp.http;
import java.io.IOException;
import java.sql.*;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import oracle.dbtools.plugin.api.di.annotations.Provides;
import oracle.dbtools.plugin.api.http.annotations.*;
import oracle.dbtools.plugin.api.routes.*;

@Provides
@Dispatches(@PathTemplate(name="plugin", value="/demos/plugin",methods={"GET", "POST"}))
class PluginDemo extends HttpServlet {
  @Inject
  PluginDemo(Connection conn, PathTemplates pathTemplates) {
    this.conn = conn;
    this.pathTemplates = pathTemplates;
  }

  public void doGet(HttpServletRequest request, 
                    HttpServletResponse response)
      throws ServletException, IOException {
      
    PathTemplateMatch match = pathTemplates
                                .matchedTemplate(request);
    try {
      /* retrieve 'who' query parameter */
      String who = match.parameters().get("who");
      who = null == who ? "anonymous" : who;
      
      /* execute database query */
      PreparedStatement ps = conn
       .prepareStatement("select sys_context('USERENV','SESSION_USER') from dual");
       
      ResultSet rs = ps.executeQuery();
      rs.next();
      
      /* determine the database user */
      String user = rs.getString(1);
      
      /* Print the greeting */
      response.getWriter().println(user + " says hello to: " + who);
      rs.close();
      ps.close();
    } catch (SQLException e) {
      throw new ServletException(e);
    }
  }

  private final Connection    conn;
  private final PathTemplates pathTemplates;
}