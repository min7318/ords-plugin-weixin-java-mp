package com.nanorose.ords.mp.handler;

import com.google.gson.Gson;
import me.chanjar.weixin.mp.api.WxMpMessageHandler;
import java.util.logging.Logger;

public abstract class AbstractHandler implements WxMpMessageHandler {

  protected final Gson gson = new Gson();
  protected Logger logger = Logger.getLogger(this.getClass().toString());

}