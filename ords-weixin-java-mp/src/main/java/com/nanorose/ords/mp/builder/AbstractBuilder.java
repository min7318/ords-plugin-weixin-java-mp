package com.nanorose.ords.mp.builder;

import com.nanorose.ords.mp.service.WxMpService;
import me.chanjar.weixin.mp.bean.message.WxMpXmlMessage;
import me.chanjar.weixin.mp.bean.message.WxMpXmlOutMessage;
import java.util.logging.Logger;

/**
 * @author Binary Wang
 */
public abstract class AbstractBuilder {
  protected final Logger logger = Logger.getLogger(this.getClass().toString());

  public abstract WxMpXmlOutMessage build(String content,
                                          WxMpXmlMessage wxMessage, WxMpService service);
}