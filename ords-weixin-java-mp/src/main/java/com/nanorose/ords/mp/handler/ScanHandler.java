package com.nanorose.ords.mp.handler;

import com.nanorose.ords.mp.builder.TextBuilder;
import me.chanjar.weixin.common.session.WxSessionManager;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.bean.message.WxMpXmlMessage;
import me.chanjar.weixin.mp.bean.message.WxMpXmlOutMessage;

import java.util.Map;


public class ScanHandler extends AbstractHandler {

  public ScanHandler(){
    
  }

  @Override
  public WxMpXmlOutMessage handle(WxMpXmlMessage wxMessage,
                                  Map<String, Object> context, WxMpService wxMpService,
                                  WxSessionManager sessionManager) {
    com.nanorose.ords.mp.service.WxMpService weixinService=
      (com.nanorose.ords.mp.service.WxMpService) wxMpService;



    //TODO 组装回复消息
    String content = "回复扫码内容";
    return new TextBuilder().build(content, wxMessage, weixinService);

  }

}