# ords-plugin-weixin-java-mp

use ORDS plugin to develop weixin mp


## Prerequisites
* [Oracle ORDS plugins](https://download.oracle.com/otndocs/java/javadocs/17.4/plugin-api/getting-started.html)
* [git](http://git-scm.com/)
* [java 8](http://java.com)
* [Maven 3](http://maven.apache.org)
* [Oracle ORDS 18.3.0.r2701456](https://www.oracle.com/technetwork/developer-tools/rest-data-services/downloads/ords-downloads-v183-5227703.html)


The artifacts can be found within the downloaded ORDS zip file in `examples/plugins/lib`

Add the necessary artifacts using the following commands (Make sure mvn is in your PATH):

```shell
cd [ojdbc8-install-dir]/ojdbc8-full
mvn install:install-file -Dfile=ojdbc8.jar -DgroupId=com.oracle -DartifactId=ojdbc8 -Dversion=18.3.0.0 -Dpackaging=jar
cd [ords-install-dir]/examples/plugins/lib
mvn install:install-file -Dfile=plugin-api-18.3.0.r2701456.jar -DgroupId=oracle.dbtools -DartifactId=plugin-api -Dversion=18.3.0.r2701456 -Dpackaging=jar
```

## Quickstart
```shell
git clone https://gitlab.com/min7318/ords-plugin-weixin-java-mp.git ords-plugin-weixin-java-mp
cd ords-plugin-weixin-java-mp
mvn clean install
```
